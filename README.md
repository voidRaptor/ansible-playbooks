# Ansible Playbooks


## Ping
```
ansible all -m ping
```


## Secrets
```
ansible-vault create <path to yml>
ansible-vault edit <path to yml>
```


## Run
```
ansible-playbook <path to playbook yml> --ask-vault-pass -K
```
